#ifndef BOUTON_H
#define BOUTON_H

#include "salle.h"
class salle;

class bouton
{
  enum ETAT
  {
    ALLUME,
    ETEINT
  };

 public:
  unsigned short int numero;
  ETAT etat;

  bouton(salle *);
  bouton(unsigned short int, salle *);
  ~bouton();

  void allumer();
  void allumer(bool);
  void eteindre();

  salle *get_parent();

 private:
  salle *parent;
};

#endif //BOUTON_H
