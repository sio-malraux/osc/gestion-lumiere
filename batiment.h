#ifndef BATIMENT_H
#define BATIMENT_H

#include <string>
#include <vector>
#include <lo/lo.h>

#include "salle.h"
class salle;

class batiment
{
 public:
  std::string nom;
  std::vector<salle *> salles;

  batiment();
  batiment(std::string);
  batiment(std::string, std::string);
  ~batiment();

  void allumer();
  void allumer(bool);
  void eteindre();

  static int allumer_handler(const char *path,
                             const char *types,
                             lo_arg **argv,
                             int argc,
                             void *data,
                             void *user_data);
 private:
  lo_server_thread server;
  std::string port;
};

#endif //BATIMENT_H
