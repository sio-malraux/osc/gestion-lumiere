#include "bouton.h"
#include <iostream>

bouton::bouton(salle *_parent) :
  bouton(0, _parent)
{}

bouton::bouton(unsigned short int _numero, salle *_parent) :
  numero(_numero),
  etat(ETEINT),
  parent(_parent)
{
  std::cout << "    Construction du bouton #"
            << numero
            << " de la salle "
            << parent->get_parent()->nom
            << parent->nom
            << std::endl;
}

bouton::~bouton() {}

void bouton::allumer()
{
  etat = ALLUME;
  std::cout << "    Allumage du bouton #"
            << numero
            << " de la salle "
            << parent->get_parent()->nom
            << parent->nom
            << std::endl;
}

void bouton::allumer(bool _etat)
{
  if(_etat == true)
  {
    allumer();
  }
  else
  {
    eteindre();
  }
}

void bouton::eteindre()
{
  etat = ETEINT;
  std::cout << "    Extinction du bouton #" << numero << std::endl;
}

salle *bouton::get_parent()
{
  return parent;
}
