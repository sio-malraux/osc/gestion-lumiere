#ifndef SALLE_H
#define SALLE_H

#include <string>
#include <vector>

#include "bouton.h"
class bouton;

#include "batiment.h"
class batiment;

class salle
{
 public:
  std::string nom;
  std::vector<bouton *> boutons;

  salle(batiment *);
  salle(std::string, batiment *);
  ~salle();

  void allumer();
  void allumer(bool);
  void eteindre();

  void set_parent(batiment *);
  batiment *get_parent();

 private:
  batiment *parent;
};

#endif //SALLE_H
