#include "batiment.h"

#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
  bool fini = false;
  batiment *bat_G = new batiment("G");
  bat_G->allumer();
  bat_G->eteindre();
  bat_G->salles[2]->boutons[1]->allumer();
  bat_G->allumer(true);
  bat_G->allumer(false);

  while(!fini)
  {
    std::this_thread::sleep_for(100ms);
  }

  delete bat_G;
  return 0;
}
