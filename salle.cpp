#include "salle.h"
#include <iostream>

salle::salle(batiment *_parent) :
  salle("Nom de salle", _parent)
{
}

salle::salle(std::string _nom, batiment *_parent) :
  nom(_nom),
  parent(_parent)
{
  std::cout << "  Construction de la salle " << parent->nom << nom << std::endl;
  boutons.push_back(new bouton(1, this));
  boutons.push_back(new bouton(2, this));
  boutons.push_back(new bouton(3, this));
}

salle::~salle() {}

void salle::allumer()
{
  std::cout << "  Allumage de la salle " << parent->nom << nom << std::endl;

  for(unsigned short int i = 0; i < boutons.size(); ++i)
  {
    boutons[i]->allumer();
  }
}

void salle::allumer(bool etat)
{
  if(etat == true)
  {
    allumer();
  }
  else
  {
    eteindre();
  }
}

void salle::eteindre()
{
  std::cout << "  Extinction de la salle " << nom << std::endl;

  for(unsigned short int i = 0; i < boutons.size(); ++i)
  {
    boutons[i]->eteindre();
  }
}

batiment *salle::get_parent()
{
  return parent;
}
