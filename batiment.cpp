#include "batiment.h"
#include <iostream>
#include <sstream>

batiment::batiment() :
  batiment("Undefined", "1234")
{}

batiment::batiment(std::string _nom) :
  batiment(_nom, "1234")
{}

batiment::batiment(std::string _nom, std::string _port) :
  nom(_nom),
  port(_port)
{
  std::cout << "Construction du bâtiment " << nom << std::endl;
  salles.push_back(new salle("141", this));
  salles.push_back(new salle("142", this));
  salles.push_back(new salle("144", this));
  server = lo_server_thread_new(port.c_str(), nullptr);
  std::stringstream output;
  output << "/" << nom << "/allumer";
  lo_server_thread_add_method(server, output.str().c_str(), "T", batiment::allumer_handler, this);
  lo_server_thread_add_method(server, output.str().c_str(), "F", batiment::allumer_handler, this);
  lo_server_thread_start(server);
}

batiment::~batiment()
{
  lo_server_thread_free(server);
}

void batiment::allumer()
{
  std::cout << "Allumage du bâtiment " << nom << std::endl;

  for(unsigned short i = 0; i < salles.size(); ++i)
  {
    salles[i]->allumer();
  }
}

void batiment::allumer(bool etat)
{
  if(etat == true)
  {
    allumer();
  }
  else
  {
    eteindre();
  }
}

void batiment::eteindre()
{
  std::cout << "Extinction du bâtiment " << nom << std::endl;

  for(unsigned short i = 0; i < salles.size(); ++i)
  {
    salles[i]->eteindre();
  }
}

int batiment::allumer_handler(const char *path,
                              const char *types,
                              lo_arg **argv,
                              int argc,
                              void *data,
                              void *user_data)
{
  batiment *moimeme = (batiment *)user_data;
  std::cout << "Allumer handler :)" << std::endl;

  if(types[0] == 'T')
  {
    moimeme->allumer();
  }
  else if(types[0] == 'F')
  {
    moimeme->eteindre();
  }

  return 0;
}
